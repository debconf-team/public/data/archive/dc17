= DebConf17 accommodation prices comparison =

Based on these sponsored attendance estimates:
* 50 during DebCamp
* 150 during DebConf

== Scenario: DebConf: everyone to hotel with optional on-site accomodation ==

This scenario assumes no hotel for DebCamp (50 people staying on-site) and
optional on-site accommodation for DebConf (100 people staying at hotel,
50 staying on-site)

Cost to rent & equip on-site venue classrooms (5 per room):
* 8,840 CAD for 10 classrooms (DebCamp & DebConf)
* 2,691 CAD for 50 Red Cross cots & pillows
  - 13.81 CAD per cot+pillow
  - 400 CAD of cloth to cover the desks
  - 1,000 CAD for shipping
  - 600 CAD for stuff we will break and need to replace
TOTAL: 11,531 CAD

Cost for hotel, triple occupancy (2 beds & 1 cot):
* 50,160 CAD for 100 people at Hôtel Universel (34 rooms, 33 paid) during DebConf (8 nights)
* 1,308 CAD for 50 Red Cross cots
  - 10.17 CAD per cot
  - 300 CAD for shipping (already shipping @ Maisonneuve)
  - 500 CAD for stuff we will break and need to replace
TOTAL: 51,468 CAD

GRAND TOTAL: 65,633 CAD

*** GLOBAL COST PP/NIGHT: 45 CAD / 34 USD / 32 EUR ***


== Old scenarios ==

=== Scenario A: everyone on-site with hotel exceptions ===

This scenario assumes 1/3 of attendees will provide valid reasons for which on-site accomodation is unworkable.

Cost to rent & equip on-site venue classrooms (5 per room):
* 5,304 CAD for 6 classrooms (DebCamp & DebConf)
* 5,712 CAD for 12 classrooms (DebConf)
* 3,943 CAD for 90 Red Cross cots & pillows
  - 13.81 CAD per cot+pillow
  - 700 CAD of cloth to cover the desks
  - 1,000 CAD for shipping
  - 1,000 CAD for stuff we will break and need to replace
TOTAL: 14,959 CAD

Cost for hotel, double occupancy :
* 9,550 CAD for 20 people at Hôtel Universel (10 rooms) during DebCamp (5 nights)
* 36,480 CAD for 50 people at Hôtel Universel (25 rooms, 24 paid) during DebConf (8 nights)
TOTAL: 46,030 CAD

GRAND TOTAL: 60,989 CAD

*** GLOBAL COST PP/NIGHT: 41 CAD / 31 USD / 28 EUR ***


=== Scenario B: everyone to hotel with optional on-site accommodation ===

This scenario assumes 20% of people will choose on-site accommodation.

Cost to rent & equip on-site venue classrooms (5 per room):
* 5,304 CAD for 6 classrooms (DebCamp & DebConf)
* 1,464 CAD for 30 Red Cross cots & pillows
  - 13.81 CAD per cot+pillow
  - 250 CAD for cloth to cover the desks
  - 500 CAD for shipping
  - 300 CAD for stuff we will break and need to replace
TOTAL: 6,768 CAD

Cost for hotel, triple occupancy (2 beds & 1 cot):
* 13,300 CAD for 40 people at Hôtel Universel (14 rooms) during DebCamp (5 nights)
* 59,280 CAD for 120 people at Hôtel Universel (40 rooms, 39 paid) during DebConf (8 nights)
TOTAL: 72,580 CAD

GRAND TOTAL: 79,348 CAD

*** GLOBAL COST PP/NIGHT: 55 CAD, 41 USD, 37 EUR ***


= Notes =

* Hotel rooms are 190 CAD/night since:
  - The base price is 160 CAD
  - We pay a 3.5% "accomodation tax"
  - We pay 15% local taxes on top
* The cost pp/night for scenario B may be smaller if we consider a number of rooms with quadruple occupation
* The hotel agreed to lend us cots (foldable beds) and cribs at no extra charge
* We should be able to bring in extra cots if the hotel runs out of theirs (TBC w/management)
* Hotel prices do not include breakfast
