FILENAME = flyer
SOURCE = ${FILENAME}.en.tex

LINGUAS ?= en fr

DC_SPECIFICS =	--msgid-bugs-address debconf-team@lists.debconf.org \
		--copyright-holder "DebConf Team" \
		--package-name "Sponsorship Brochure" \
		--package-version "$(shell TZ=UTC date +%Y-%m-%d-%H%M%S)" \
		--master-charset UTF-8 \
		--keep 0 # Default is 80%

default: all

${SOURCE}:
	# Don't try to build the source file

l10n/messages.pot:
	[ -f $@ ] || touch $@
.PRECIOUS: l10n/messages.pot

l10n/%.po: l10n/messages.pot ${SOURCE}
	po4a ${DC_SPECIFICS} --force --no-translations po4a.cfg
	msgcat --width=79 $@ -o $@
	sed -i '/^"POT-Creation-Date:/ d' $@
	touch $@ # po4a touches $@ after $<
	[ -f $@ ] || cp $< $@

${FILENAME}.%.tex: l10n/%.po ${SOURCE}
	po4a ${DC_SPECIFICS} po4a.cfg
	sed -i '/^"POT-Creation-Date:/ d' $<
.PRECIOUS: ${FILENAME}.%.tex

${FILENAME}.%.pdf: ${FILENAME}.%.tex
	pdflatex $^
	pdflatex $^

ALL_SOURCES = $(foreach lang,${LINGUAS},${FILENAME}.${lang}.tex)
ALL_PDFS = $(patsubst %.tex,%.pdf,${ALL_SOURCES})
ALL_AUTOGEN_SOURCES = $(filter-out ${SOURCE},${ALL_SOURCES})

all: ${ALL_PDFS}

clean:
	git clean -fX -- .

veryclean: clean
	git clean -f -- .

.PHONY: all clean veryclean update-po
